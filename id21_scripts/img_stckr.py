"""
Example of code to stack maps from FluoXAS without doing the fit
Dependencies: python -m pip install numpy h5py pymca
"""
import os
import h5py
import numpy as np
from contextlib import contextmanager


### Input data for this code
mainh5path = "/data/visitor/ma3262/id21/Bov_S03/Bov_S03_roi29967_36623/Bov_S03_roi29967_36623.h5"  # Path and name of the FluoXAS h5 file
outpath    = "."  # Path of the directory to write the output file ("." is the current working directory)
elemdet    = "ZnKa_corr_norm0"  # Select the element or the detectors with data, "idet", "TiKa_corr_norm0", etc
dim1       = 100  # first dimension of the map
dim2       = 40   # second dimension of the map
energycntr = "enetraj"   # Select energy counter 
outname    = "stacked_roi29967_36623"  # Name for the output file


### Start of all the functions for this code
@contextmanager
def openh5(filename):
    os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
    with h5py.File(filename, mode="r") as f:
        yield f

def fileselection():
    with openh5(mainh5path) as f:
        first_keys = list(f.keys())
        #print("file name = ", first_keys)
        for i in range(len(first_keys)):
            file_name = str(first_keys[i])
            #print("file name as list = ", file_name)
            try:
                all_data_files = f[file_name + "/measurement"]
            except Exception as ex:
                print("The error exception in fileselection() is = ", ex)
                badfiles.append(file_name)
                continue

            #print("file name with measurement = ", all_data_files)
            if (elemdet in all_data_files):
                goodfiles.append(file_name)
            else:    
                badfiles.append(file_name)

def readmaps(eachfile):
    with openh5(mainh5path) as f:
        data = f[eachfile + "/measurement"]
        element = data[elemdet][:]
        energy_map = "%.4f" % f[eachfile]['instrument']['positioners'][energycntr][()] 
        print("Energy = ", energy_map)
        energy_map_lst.append(energy_map)
    return element

def create_stack(element, i, stack):
    element = [float(x) for x in element]
    element = np.array(element)
    element = element.reshape(dim1,dim2)
    #print("element", i, element)
    if i == 0:
        stack = element
    else:
        stack = np.dstack((stack, element))
    return stack

def saveh5(stack, energy_map_lst):
    dm1 = np.arange(dim1)
    dm2 = np.arange(dim2)
    stack = np.transpose(stack, (2,0,1))
    print("Stack shape is = ", stack.shape)

    f = h5py.File(outpath + outname + ".h5", "w")  # create the HDF5 NeXus file
    nxentry = f.create_group('Results')
    nxentry.attrs["NX_class"] = 'NXentry'
    nxdata = nxentry.create_group('Stack')
    nxdata.attrs["NX_class"] = 'NXdata'

    nxdata.create_dataset('dim1', data=dm1)
    nxdata.create_dataset('dim2', data=dm2)
    nxdata.create_dataset('roi signal', data=stack)
    nxdata.create_dataset(energycntr, data=energy_map_lst)
    f.close()


# Files selection and starting lists
goodfiles  = []
badfiles   = []
energy_map_lst = []

fileselection()
goodfiles = [float(x) for x in goodfiles]
goodfiles = sorted(goodfiles)
goodfiles = [str(x) for x in goodfiles]
#print("good files = ", goodfiles)

if outpath[-1] != "/":
    outpath = outpath + "/"

### Big loop to stack the maps
for i in range(len(goodfiles)):
    try:
        element = readmaps(goodfiles[i])
        #print("element", i, element)
        if i == 0:
            stack = None
        stack = create_stack(element, i, stack)
        #print("stack", stack)
    except Exception as ex:
        print("The error in the main loop is = ", ex)
        del energy_map_lst[i]
        badfiles.append(goodfiles[i])
        continue
    
# Create and save the output h5 file  
energy_map_lst = sorted(energy_map_lst)
energy_map_lst = [float(x) for x in energy_map_lst]  
energy_map_lst = np.array(energy_map_lst)
saveh5(stack, energy_map_lst)

# Print the list of the scans with bad data
if len(badfiles) > 0:
    print("")
    print("Scans with bad data:")
    print(badfiles[1:])
    print("")
