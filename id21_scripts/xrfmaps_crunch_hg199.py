import os
import re
import h5py
import numpy
import logging
from glob import glob
from pprint import pprint
from contextlib import contextmanager

from spectrocrunch.pipelines.run import run_sequential
from PyMca5.PyMcaIO.ConfigDict import ConfigDict
from spectrocrunch.process import utils
from spectrocrunch.io import nxfs

logging.basicConfig(level=logging.INFO)

# Main input data for this code
dir_in  = "/data/visitor/hg199/id21/20221125/P2_C4_EMdef2T_ts/" # Marine Path name of directory to find raw data with the rois
dir_out = "/data/visitor/hg199/id21/20221125/processed/fluoxas/" # Path of the directory to write all the maps fitted ("." is the current working directory)
cfgfilename = "/data/visitor/hg199/id21/20221125/pymca/config_9_2_batch.cfg" # Path and name of the config file. cfgfilename = None for stack the counters without the XRF fitting
filtersample = ["P2_C4_EMdef2T_ts_roi44392_52777.h5"] # Leave empty [""] to fit all the files inside the folder or write ["specific_sample_name.h5"] to run only one specific file

# Advance or extra options for this code
beamline = "id21" # "id21" or "id13"
normalization_counter = "iodet" # None when you don't want normalization, "iodet" when you do want normalization
normalization_target = None # None by default or a number if you want to do (normalization_target)/(normalization_counter)
stack_positioner = "enetraj" # None when you want to fit maps or energy motor ("enetraj") in the FluoXAS case
quantification = False # True if you want to do a quantification fit, you need the proper config file, False otherwise
fast_fitting = True # True to perform a fast fitting False otherwise
reference = "Cu_K"  # Reference element for FluoXAS alignment, E.g = "Si_K". To run it with no reference = None
ignore_scans =  [] # Specific scans to ignore in the FluoXAS case, scans with errors to skip
fluo_counters = [] # List of counters to stack without need to do the fit E.g. ["SiKa", "PKa", "CaK"]. leave empty for other cases fluo_counters = []


###### EXAMPLES below of diferent runs, do not modify them unless you copy the file in your own folder

# Different energies XRF maps from ID21
if False:
    beamline = "id21"
    dir_in = "/data/id21/inhouse/scripts/examples/BNBT6_oxJ_NP/"
    dir_out = "/data/id21/inhouse/scripts/examples/out/"
    filtersample = ["roi15834"]
    cfgfilename = {5.1:"/data/id21/inhouse/scripts/examples/pymca/5100_leia_batch.cfg", 
                   5.300:"/data/id21/inhouse/scripts/examples/pymca/5300_leia_batch.cfg", 
                   6.00:"/data/id21/inhouse/scripts/examples/pymca/6000_leia_batch.cfg", 
                   0:"/data/id21/inhouse/scripts/examples/pymca/6000_leia_batch.cfg"}   # config files for different energies maps in directory form
    normalization_counter = "iodet"
    stack_positioner = None
    quantification = False 
    fast_fitting = True 
    ignore_scans = []
    reference = None 
    fluo_counters = []
    
# XRF maps to be stiched at ID13
if False:
    beamline = "id13"
    dir_in = "/data/visitor/hg172/id13/MC_BMM083/MC_BMM083_907_12205/"
    dir_out = "/data/id21/inhouse/scripts/examples/out/"
    filtersample = [""]
    cfgfilename = "/data/visitor/hg172/id13/XXPROCESS_XRF/MC/config_BMM083_batch.cfg"
    normalization_counter = "ct24"
    stack_positioner = "enetraj"
    quantification = False 
    fast_fitting = True 
    ignore_scans = []
    reference = None  
    fluo_counters = []

# FluoXAS map at ID21   
if False:
    beamline = "id21"
    dir_in  = "/data/id21/inhouse/20Sep/blc12369/id21/CS5_bottom_fluoXAS_Cl/CS5_bottom_fluoXAS_Cl_0001/" 
    dir_out = "/data/id21/inhouse/scripts/examples/out/"
    filtersample = ["CS5_bottom_fluoXAS_Cl_0001"] 
    cfgfilename = "/data/id21/inhouse/20Sep/blc12369/id21/pymca/CS5_2_85_fluoXAS.cfg"
    cfgfilename = None # Using cfgfilename = None so it won't do the XRF fit
    normalization_counter = "iodet"
    stack_positioner = "enetraj"
    quantification = False 
    fast_fitting = True  
    ignore_scans =  [3] + list(range(5, 105)) # Specific scan third, [3] and a range from 5 to 105 
    reference = "Si_K"  # Reference element for FluoXAS alignment, E.g = "Si_K". To run it with no reference = None
    fluo_counters = ["SiKa", "PKa", "CaK"] # List of counters to stack without need to do the fit E.g. ["SiKa", "PKa", "CaK"]. leave empty for other cases fluo_counters = []

##### Start of all the functions
@contextmanager
def openh5(filename):
    os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
    with h5py.File(filename, mode="r") as f:
        yield f
        
        
def subfolders_search(dir_in):
    #print("dir_in = ", dir_in )
    subfolders = [f.path for f in os.scandir(dir_in) if f.is_dir()]
    #print("len(subfolders) = ", len(subfolders))
    #print("subfolders1 = ", subfolders)
    subfolders = [
        filename 
        for filename in subfolders 
        if any(substring in filename for substring in ["_roi"])
    ]
    if len(subfolders) == 1:
        #print("subfolders[0] = ", subfolders)
        subfolder_check = subfolders[0].split('/')
        if subfolder_check[-1] == "": 
            if not any(substring in subfolder_check[-2].lower() for substring in ["_roi"]):
                #print("subfolders2 = ", subfolders)
                subfolders = []
                subfolders = [dir_in]
        else:
            if not any(substring in subfolder_check[-1].lower() for substring in ["_roi"]):
                #print("subfolders2 = ", subfolders)
                subfolders = []
                subfolders = [dir_in]
                
    if len(subfolders) < 1:
        subfolders = []
        subfolders = [dir_in]
    #print("subfolders3 = ", subfolders)
    #print("Number of folder with rois = ", len(subfolders))
    #print("subfolders with rois :")
    #pprint(subfolders)
    return subfolders
        

def files_in_folder(dir_in, filtersample):
    filenames = glob(os.path.join(dir_in, "*.h5"))
    filenames = [
        filename
        for filename in filenames
        if any(substring in filename for substring in filtersample)    
    ]
    #print("Number of h5 files in folder =", len(filenames))
    #print("Filtered files from folder =")
    #pprint(filenames)
    filtered_files = []

    for basename in filenames:
        filename = os.path.join(dir_in, basename)
        with openh5(filename) as f:
            scans = list(f.keys())
            datacheck = f[scans[0]]
            if "title" in datacheck.keys():
                filtered_files.append(filename)
    return filtered_files


def id13_xrf_maps(file_in, ignore_scans):
    with openh5(file_in) as f:
        good_maps = []
        ignore_scans = [s if isinstance(s, str) else f"{s}.1" for s in ignore_scans]
        yis = []
        diffyis = 0
        scans = list(map(float, f.keys()))
        scans = [str(scan) for scan in sorted(scans)]
        print(" Number of scans =", len(scans))
        print(" Scans =", scans)

        for scan in scans:
            if scan in ignore_scans:
                continue
            try:
                datacheck = f[scan]
                datacheck = datacheck["title"][()]
                datacheck = str(datacheck)
                datacheck = re.sub(" ", "", datacheck)
                datacheck = datacheck.split(",")
                # print("each key= ", scan)
                if "akmap_lut" in datacheck[0]:

                    yi = datacheck[1]
                    yis.append(yi)
                    # yf = datacheck[2]
                    # zi = datacheck[5]
                    # zf = datacheck[6]
                    # print("XRF map data found =", datacheck, scan)
                    # print("yi= ", yi, scan)
                    good_maps.append(scan)

            except Exception as ex:
                print(f" Error checking scan {scan}: {ex}")
                continue

        if len(yis) > 1:
            for i in range(len(yis)):
                if yis[i] > yis[i - 1]:
                    diffyis += 1

            if diffyis == 0:
                tile_shape = (1, int(len(yis)))
            else:
                shape1 = int(len(yis) - diffyis)
                shape2 = int(len(yis) / shape1)
                tile_shape = (shape2, shape1)
                # print("Values greater that the one before =", diffyis)
                # print("Length of initial motors list =", len(yis))
                # print("Initial values of  motor y=", yis)
        else:
            tile_shape = (1, 1)

        print(" Files with XRF maps =", good_maps)
        print(" Tile shape =", tile_shape)
    return good_maps, tile_shape


def id21_xrf_maps(file_in, ignore_scans):
    good_maps = list()
    ignore_scans = [s if isinstance(s, str) else f"{s}.1" for s in ignore_scans]
    with openh5(file_in) as f:
        for scan in f:
            if scan in ignore_scans:
                continue
            title = f[scan]["title"][()]
            if isinstance(title, bytes):
                title = title.decode()
            if "mesh" in title or "l2scan" in title:
                good_maps.append(scan)
    return good_maps, None


def get_pymca_time(cfgfilename):
    """Get exposure time from PyMca config file"""
    cfg = ConfigDict()
    cfg.read(cfgfilename)
    return cfg["concentrations"]["time"]


def pymca_fit(tasks, uris, cfgfilename, outfilename, quantification, fast_fitting, counters, fluo_counters=tuple()):
    """Fit and reshape/stack XRF maps"""
    pymcatasks = []
    if cfgfilename:
        preset_time = get_pymca_time(cfgfilename)
    else:
        preset_time = numpy.nan
    if beamline == "id13":
        detectors = ["xmap2_det0"]
    elif beamline == "id21":
        detectors = ["fx2_det0"]
    else:
        raise ValueError(f"beamline {repr(beamline)} is unknown")
    
    for uri in uris:
        for name in fluo_counters:
            for det in detectors:
                if (det + "/" + name) not in counters: # For not to repeat the counters
                    counters.append(det + "/" + name)

        parameters = {
            "method": "blissmcapre",
            "outputparent": outfilename + "::/" + uri.split("/")[-1],
            "uri": uri,
            "detectors": detectors,
            "preset_time": preset_time,
            "add": True,
            "dtcor": True,
            "dependencies": nxfs.factory(uri),
            "counters": counters,
        }
        task = utils.create_task(**parameters)
        tasks.append(task)

        parameters = {
            "method": "blisspymca",
            "dependencies": task,
            "pymcacfg": cfgfilename,
            "quant": quantification,
            "diagnostics": False,
            "fastfitting": fast_fitting,
        }
        task = utils.create_task(**parameters)
        tasks.append(task)
        pymcatasks.append(task)
    return pymcatasks


def id13_shape_parser(cmd):
    """XRF map shape from command"""
    cmd = cmd.replace(" ", "")
    parts = cmd.split(",")
    return int(parts[3]), int(parts[7])


def tile(tasks, dependencies, outfilename, tile_shape):
    parameters = {
        "method": "tile",
        "dependencies": dependencies,
        "outputparent": outfilename + "::/tiled_fit",
        "tile_shape": tile_shape,
    }
    if beamline == "id13":
        parameters["shape_parser"] = id13_shape_parser
    task = utils.create_task(**parameters)
    tasks.append(task)
    return task


def stack(tasks, dependencies, outfilename, stack_positioner):
    parameters = {
        "method": "stack",
        "dependencies": dependencies,
        "outputparent": outfilename + "::/fitted_stack",
        "stack_positioner": stack_positioner,
    }
    if beamline == "id13":
        parameters["shape_parser"] = id13_shape_parser
    task = utils.create_task(**parameters)
    tasks.append(task)
    return task


def normalize(tasks, dependencies, counter, target):
    """Normalize PyMca results"""
    copy = [{"method": "regex", "pattern": "^.+_errors$"}]
    if target is None:
        target = f"mean({{{counter}}})"
    parameters = {
        "method": "expression",
        "name": "normalize",
        "dependencies": dependencies,
        "expression": f"{{}}*{target}/{{{counter}}}",
        "copy": copy,
    }
    task = utils.create_task(**parameters)
    tasks.append(task)
    return task


def crop(tasks, dependencies, reference):
    """Crop aligned results"""
    parameters = {
        "method": "crop",
        "name": "crop",
        "dependencies": dependencies,
        "nanval": numpy.nan,
        "reference": reference,
    }
    task = utils.create_task(**parameters)
    tasks.append(task)
    return task


def align(tasks, dependencies, reference):
    """Align PyMca results"""
    parameters = {
        "method": "align",
        "dependencies": dependencies,
        "alignmethod": "elastix",
        "reference": reference,
        "refimageindex": "middle",
        "crop": False,
        "plot": True,
    }
    task = utils.create_task(**parameters)
    tasks.append(task)
    return task


def config_file_dict(filename):
    with openh5(filename) as f:
        energ= "nan"
        find_energies = f['1.1']['instrument']['positioners'].keys()
        if "enetraj_und" in find_energies:
            energ = "%.3f" % f['1.1']['instrument']['positioners']['enetraj_und'][()] 
            energN = "enetraj_und"                                                     
        if energ == "nan":  
            if "eneund" in find_energies:
                energ = "%.3f" % f['1.1']['instrument']['positioners']['eneund'][()]
                energN = "eneund"
        if energ == "nan":
            if "enetraj" in find_energies:
                energ = "%.3f" % f['1.1']['instrument']['positioners']['enetraj'][()]
                energN = "enetraj"
        if energ == "nan":  
            if "dcmene" in find_energies:
                energ = "%.3f" % f['1.1']['instrument']['positioners']['dcmene'][()]
                energN = "dcmene"
        if energ == "nan":  
            if "enmonound" in find_energies:
                energ = "%.3f" % f['1.1']['instrument']['positioners']['enmonound'][()]
                energN = "enmonound"
        if energ == "nan":  
            if "enmono" in find_energies:
                energ = "%.3f" % f['1.1']['instrument']['positioners']['enmono'][()]
                energN = "enmono"
        print("Energy: %.3f keV read from: '%s'" % (float(energ), energN))
        cfgfilename_dict = cfgfilename[float(energ)]
        print("Config file chosen = ", cfgfilename_dict)
    return cfgfilename_dict


def process(
    filename,
    scan_numbers,
    outfilename,
    cfgfilename,
    quantification,
    fast_fitting,
    normalization_counter=None,
    normalization_target=None,
    tile_shape=None,
    stack_positioner=None,
    reference=None,
    fluo_counters=tuple(),
):
    """Create data processing pipeline and execute it"""
    tasks = []
    uris = []
    for i in range(len(scan_numbers)):
        uris.append(filename + "::/" + scan_numbers[i])
    if normalization_counter:
        counters = [normalization_counter]
    else:
        counters = []
    dependencies = pymca_fit(tasks, uris, cfgfilename, outfilename, quantification, fast_fitting, counters=counters , fluo_counters=fluo_counters)
    if tile_shape:
        dependencies = tile(tasks, dependencies, outfilename, tile_shape)
    elif stack_positioner:
        dependencies = stack(tasks, dependencies, outfilename, stack_positioner)
    else:
        dependencies = stack(tasks, dependencies, outfilename, None)
    if normalization_counter:
        dependencies = normalize(tasks, dependencies, normalization_counter, normalization_target)
    if len(scan_numbers) > 1:
        if reference:
            dependencies = align(tasks, dependencies, reference)
            dependencies = crop(tasks, dependencies, reference)
    run_sequential(tasks, name=outfilename)
    

###### Starts the use of all the functions
results = dict()
subfolder = subfolders_search(dir_in)
for i in range(len(subfolder)):
    #print("subfolder[i] = ", str(subfolder[i]))
    filtered_files = files_in_folder(subfolder[i], filtersample)
    
    for filename in filtered_files:
        #print("filenameeeeee = ", filename)
        basename = os.path.basename(filename)
        print("\nPROCESING THE FILE:", repr(basename))

        if beamline == "id13":
            scan_numbers, tile_shape = id13_xrf_maps(filename, ignore_scans)
        elif beamline == "id21":
            scan_numbers, tile_shape = id21_xrf_maps(filename, ignore_scans)
        else:
            raise ValueError(f"beamline {repr(beamline)} is unknown")
        if not scan_numbers:
            print(f"Skip {repr(basename)}: no XRF maps")
            results.setdefault("No XRF maps", list()).append(basename)

        try:
            if cfgfilename is None: 
                cfgfilename_dict = None
            elif isinstance(cfgfilename, dict):
                cfgfilename_dict = config_file_dict(filename)
            else:
                cfgfilename_dict = cfgfilename
            outfilename = os.path.join(dir_out, "fitted_" + basename)
            print("File name path = ", filename)
            process(
                filename,
                scan_numbers,
                outfilename,
                cfgfilename=cfgfilename_dict,
                quantification=quantification,
                fast_fitting=fast_fitting,
                normalization_counter=normalization_counter,
                normalization_target=normalization_target,
                tile_shape=tile_shape,
                stack_positioner=stack_positioner,
                reference=reference,
                fluo_counters=fluo_counters,
            )
            results.setdefault("Succeeded", list()).append(basename)

        except Exception as ex:
            print(f"Error processing file {repr(basename)}: {ex}")
            results.setdefault("Failed", list()).append(basename)
            continue

print("\nFINISHED")
print("Results are saved in:", repr(dir_out))
for rtype, lst in results.items():
    print(f"\nFiles {rtype} (number of files = {len(lst)})")
    pprint(lst)
