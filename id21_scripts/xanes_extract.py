"""
Example for batch extract of XANES spectra
Dependencies: python -m pip install numpy h5py pymca pandas
"""
import os
import re
import h5py
import numpy as np
import pandas as pd
from pprint import pprint
import matplotlib.pyplot as plt
from contextlib import contextmanager
from scipy.interpolate import Akima1DInterpolator
from PyMca5.PyMcaPhysics.xas.XASNormalization import XASNormalization


### Input data for this code
 
mainh5path  = "/data/visitor/hg199/id21/20221125/hg199_id21.h5"  # Path and name of the main h5 file
outpath     = "/data/visitor/hg199/id21/20221125/processed/xanes/"  # Path to save all the ouput data ("." is the current working directory)
matchers    = [""]  # Filter to fit files with the matching name (e.g. "sample_1" or "region3"). Leave empty ("") to fit all
elemdet     = "fx2_det0_CuKa"  # Select the element and detector with data
elemdetnorm = "CuKa_corr_norm0"  # Select the element and counter with the normalization and dead time correction
energycntr  = "enegoat"  # Select the energy counter

# Saving options
save_singles  = "no"  # Write "yes" to save each XANES spectra in a single .dat file for each of them "no" otherwise
save_average  = "yes"  # Write "yes" to create files with the average of poi with several scans "no" otherwise
save_orange   = "yes"  # Write "yes" to create a single .csv file with all XANES spectrum "no" otherwise
orange_name   = "Cu_hg199_orange"  # Name for the orange output file 
save_excel    = "yes"  # Write "yes" to create a single .csv file with all XANES spectrum in columns layout "no" otherwise
excel_name    = "Cu_hg199_xanes"  # Name for the excel output file
add_positions = "no"  # Write "yes" to add the motor positions at the end of the orange and excel files and "Jump" value "no" otherwise

# Extra options for filtering XANES spectra
wlbkgratio = 4.0  # Ratio for the filter between the max of the white line and the mean of the pre edge background
nopoibckg  = 8    # Number of points to take the mean of the background level
trhnpoi    = 50   # Treshold to filter the data with less number of points in the spectrum than trhdnpoi

# Interpolation 
interpolate   = "no"   # Write "yes" to perform an interpolation for every XANES spectrum "no" otherwise
energy_start  = 8.96    # First energy value of the XANES spectra in keV
energy_finish = 9.298   # Last energy value of the XANES spectra in keV
energy_step   = 0.0005  # Energy step size for the final interpolation spectra in keV

# Options for filtering experiments with more than one element (Cr, Mn, Fe, etc) or to cut the spectrum range
two_plus_elem   = "no"  # Write "yes" if you have XANES data for more than one element "no" otherwise
lwr_lmt_energy  = 8.96   # Lower limit for the energy of the XANES spectra to cut (this limit has to be inside the range of the experimental XANES)
hghr_lmt_energy = 9.298  # Higher limit for the energy of the XANES spectra to cut (this limit has to be inside the range of the experimental XANES)

# Normalization values
norm_choosing = "no" # Write "yes" to have all XANES data normalized using PyMca functions "no" otherwise 
edge = 9.0160     # Values taken from the PyMca GUI, XAS normalization option
pre  = "Constant", -0.036, -0.01188
post = "Linear", 0.0946, 0.179

# Define another roi to take the Fluorescence signal from instead of the already defined
define_new_roi = "no"        # Write "yes" to redefine the roi of channels to take the fluorescence signal from, "no" otherwise
elemdetfull    = "fx2_det0"  # Select fluo detector with all the data 
roistarts      = 1045        # First channel to take the roi from
roiends        = 1103        # Last channel to take the roi from


### Start of all the functions for this code

@contextmanager
def openh5(filename):
    os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
    with h5py.File(filename, mode="r") as f:
        yield f


def fileselection():
    with openh5(mainh5path) as f:
        first_keys = list(f.keys())
        counter = 0
        xanesinseq = []
        counterseq= 0 
        #print("file name = ", first_keys)
        for i in range(len(first_keys)):
            file_name = str(first_keys[i])
            #print("file name as list = ", file_name)
            try:
                all_data_files = f[file_name + "/measurement"]
                datacheck = f[file_name]
                datacheck = datacheck["title"][()]
                datacheck = str(datacheck)
            except Exception as ex:
                print("The error exception in fileselection() is = ", ex)
                badfiles.append(file_name)
                continue
            
            if ("sequence_of_scans" in datacheck):
                counterseq += 1 
                xanesinseq.append(counter)
                counter = 0
                #print("SEQUENCE = ", datacheck)
            #print("file name = ", all_data_files)
            if ((elemdet in all_data_files) and (energycntr in all_data_files)):
                counter += 1
                goodfiles.append(file_name)
            else:    
                badfiles.append(file_name)
                
        xanesinseq.append(counter)
        xanesinseq = xanesinseq[1:]
        #print("List with number of XANES from each sequence scan = ", xanesinseq)
        return xanesinseq


def fileselectiontags():
    with openh5(mainh5path) as f:
        first_keys = list(f.keys())
        counter = 0
        xanesinseq = []
        counterseq= 0 
        
        for i in range(len(first_keys)):
            file_name = str(first_keys[i])
            try:
                datacheck = f[file_name]
                datacheck = datacheck["title"][()]
                datacheck = str(datacheck)
                #print("XANES or sequence of scans check = ", datacheck)
            except Exception as ex:
                print("The error exception in fileselectiontags() is = ", ex)
                badfiles.append(file_name)
                continue
            
            if ("sequence_of_scans" in datacheck):
                counterseq += 1 
                xanesinseq.append(counter)
                counter = 0
                #print("SEQUENCE = ", datacheck)
            elif (("zaptraj enetraj" in datacheck) or ("ascan enmonound" in datacheck) or ("contscan" in datacheck)):
                counter += 1
                #print("XANES = ", datacheck)
                goodfiles.append(file_name)
                
        xanesinseq.append(counter)
        xanesinseq = xanesinseq[1:]
        #print("List with number of XANES from each sequence scan = ", xanesinseq)
        return xanesinseq


def readspectrum(eachfile):
    with openh5(mainh5path) as f:
        data = f[eachfile + "/measurement"]
        
        if elemdetnorm in data:
            names = [energycntr, elemdetnorm]
        elif energycntr and elemdet and "iodet" and "fx2_det0_fractional_dead_time" in data:
            #print("Elemdetnorm not found, corr_norm done in this code = ", eachfile)
            names = [energycntr, elemdet, "iodet", "fx2_det0_fractional_dead_time"]
        else:
            print("Element det_norm not found and not normalized = ", eachfile)
            names = [energycntr, elemdet]
            
        npoints = min(data[name].shape[0] for name in names)
        energy  = data[energycntr][:npoints]

        # Checking for element correction normalization
        if define_new_roi == "yes":
            newroi = 0
            element_roi = data[elemdetfull][:npoints]
            for i in range(roiends-roistarts+1):
                count = roistarts + i
                newroi += element_roi[:,count]
                spectrum = newroi
            iodet = data["iodet"][:npoints]
            det0_dead_time = data["fx2_det0_fractional_dead_time"][:npoints]
            absorb_norm0 = (spectrum / (iodet * (1 - det0_dead_time)))
            spectrum = absorb_norm0
        elif elemdetnorm in data:
            spectrum = data[elemdetnorm][:npoints]
            #print("element Ka norm present")
        elif energycntr and elemdet and "iodet" and "fx2_det0_fractional_dead_time" in data:   
            iodet = data["iodet"][:npoints]
            #print('iodet = ', iodet)
            det0 = data[elemdet][:npoints]
            #print('det0 = ', det0)
            det0_dead_time = data["fx2_det0_fractional_dead_time"][:npoints]
            #print('det0_dead_time = ', det0_dead_time )
            absorb_norm0 = (det0 / (iodet * (1 - det0_dead_time)))
            #print('absorb_norm0 = ', absorb_norm0)
            spectrum = absorb_norm0
            #print('spectrum = ', spectrum)
        else:
            spectrum = data[elemdet][:npoints]
    return energy, spectrum


def averaging():
    n = -1
    avgenergylist = []
    avgspectrumlist = []
    avgfilenamelist = []
    print("")
    #print("All scans names in the main h5 file = ", len(xanesinseq))
    #print("Number of repeats on each scan = ", xanesinseq)
        
    for i in range(len(xanesinseq)):
        #print("i = ", i)
        avgspectrum = [0]
        for j in range(xanesinseq[i]):
            try:
                #print("j = ", j)
                n += 1
                #print("Next file is = ", filesforavg[n])
                energy, spectrum = readspectrum(filesforavg[n])
                if len(avgspectrum) > 1:
                    minno = min(len(avgspectrum), len(spectrum), len(energy))
                    avgspectrum = avgspectrum[:minno]
                    spectrum = spectrum[:minno]
                    energy = energy[:minno]
                avgspectrum = np.add(avgspectrum, spectrum)
            except Exception as ex:
                print("The error exception in averaging() is = ", ex)
                continue
    
        if not int(xanesinseq[i]) <= 1:
           avgspectrum = avgspectrum/(int(xanesinseq[i]))
           avgfilename = "AVG_" + filesforavg[n]
           avgenergylist.append(energy)
           avgspectrumlist.append(avgspectrum)
           avgfilenamelist.append(avgfilename)
           # plot(energy, avgspectrum, avgfilename) # Plotting each XANES averaged spectra
    return avgenergylist, avgspectrumlist, avgfilenamelist
    
    
def validatespectrum():
    validated = None
    if ((len(energy) < trhnpoi) or (len(spectrum) < trhnpoi) or (all(elem == spectrum[5] for elem in spectrum))):
        print("(Energy or spectrum) < (trhdnpoi or filter_for_energy) or all elements are the same")
        validated = False
    
    choosing = max(spectrum) / np.mean(spectrum[2:nopoibckg])
    choosing = abs(choosing)
    print("Edge/background ratio = ", choosing)
    #print("sigma", (spectrum[:nopoibckg]).std())
    
    if (choosing >= wlbkgratio):
        validated = True
    return validated
        
    
def plot(energy, spectrum, filename):
    plt.figure().suptitle(filename)
    plt.plot(energy, spectrum)
    plt.show()
    
    
def add_positioners(spectrum, eachfile):
    with openh5(mainh5path) as f:
        if "AVG_" in eachfile:
            eachfile = eachfile[4:]
        positioners_val = f[eachfile + "/instrument/positioners"]
        samy  = positioners_val["samy"][()]
        samz  = positioners_val["samz"][()]
        sampy = positioners_val["sampy"][()]
        sampz = positioners_val["sampz"][()]
        spectrum = np.append(spectrum, "")
        spectrum = np.append(spectrum, samy)
        spectrum = np.append(spectrum, samz)
        spectrum = np.append(spectrum, sampy)
        spectrum = np.append(spectrum, sampz)
    return spectrum


def normalization(energy, spectrum, edge, pre, post):
    polynomial_map = {
        "Modif. Victoreen": -2,
        "Victoreen": -1,
        "Constant": 0,
        "Linear": 1,
        "Parabolic": 2,
        "Cubic": 3,
    }
    prename, premin, premax    = pre
    postname, postmin, postmax = post

    pre_edge_regions  = [[premin, premax]]
    post_edge_regions = [[postmin, postmax]]
    algorithm = "polynomial"
    algorithm_parameters = {
        "pre_edge_order": polynomial_map[prename],
        "post_edge_order": polynomial_map[postname],
    }

    result = XASNormalization(
        spectrum=spectrum,
        energy=energy,
        edge=edge,
        pre_edge_regions=pre_edge_regions,
        post_edge_regions=post_edge_regions,
        algorithm=algorithm,
        algorithm_parameters=algorithm_parameters,
    )
    (
        energy,
        normalizedSpectrum,
        edge,
        jump,
        pre_edge_function,
        prePol,
        post_edge_function,
        postPol,
    ) = result
    preLine  = pre_edge_function(prePol, energy)
    postLine = post_edge_function(postPol, energy)
    return normalizedSpectrum, jump, preLine, postLine
    
    
def savedata(energy, spectrum, file_name):
    combined  = np.vstack((energy, spectrum)).T
    np.savetxt(outpath + file_name + ".dat", combined, fmt='%10.8f')


def add_col(path_in, lst_col, path_out):
    file_in = open(path_in, "r")
    in_line_lst = file_in.readlines()
    file_in.close()

    file_out = open(path_out, "w")
    for position, str_line in enumerate(in_line_lst):
        wrstring = str(lst_col[position] + ',') + str_line
        file_out.write(wrstring)
    file_out.close()
   

# Adding "/" at the end of the output path if missing and removing caps
if outpath[-1] != "/":
    outpath = outpath + "/"
# Matching the energies for the interpolation and the cutting 
if two_plus_elem == "yes":
    if interpolate == "yes":
        if energy_start <= lwr_lmt_energy:
            energy_start = lwr_lmt_energy + energy_step
        if energy_finish > hghr_lmt_energy:
            energy_finish = hghr_lmt_energy
save_singles = save_singles.lower()
save_average = save_average.lower()
save_orange = save_orange.lower()
save_excel = save_excel.lower()
add_positions = add_positions.lower()
norm_choosing = norm_choosing.lower()

# File selection and starting lists
goodfiles  = []
badfiles   = []
badonesfil = []
errorfiles = []
goodones   = 0
badones    = 0
errorones  = 0
orange_file = []
first_csv_column = ["Energy(keV)"]

#fileselection() # Other way to find the data with XANES spectra
xanesinseq = fileselectiontags()
filesforavg = goodfiles
goodfiles = [s for s in goodfiles if (any(xs in s for xs in matchers))]

energy_forcsv = []
energy_forcsv = np.array(energy_forcsv)


### Big loop for the XANES scans
for i in range(len(goodfiles)):  
    try: 
        # Read data
        print("")
        print("File = ", goodfiles[i])
        energy, spectrum = readspectrum(goodfiles[i])

        # Two + elements
        if two_plus_elem == "yes":
            cut_energy = []
            cut_spectrum = []
            for j in range(len(energy)):
                if ((energy[j] >= lwr_lmt_energy) and (energy[j] <= hghr_lmt_energy)): 
                    cut_energy.append(energy[j]) 
                    cut_spectrum.append(spectrum[j])
            cut_spectrum = [float(x) for x in cut_spectrum]
            energy = cut_energy
            spectrum = cut_spectrum
            energy = np.array(energy)
            spectrum = np.array(spectrum)
            if (len(energy) < 5):
                continue
        
        # Validate the spectrum
        if not validatespectrum():
            print("Noooooooooooooooooooooot Vaaaaaaaaaaaaaalid")
            badones += 1
            badonesfil.append(goodfiles[i])
            #sys.exit(1)
            continue

        # Interpolating spectra
        if interpolate == "yes":
            energy_range = np.arange(energy_start, energy_finish, energy_step)
            interp_func = Akima1DInterpolator(energy, spectrum)
            interpolated_spectrum = interp_func(energy_range)
            energy = energy_range
            spectrum = interpolated_spectrum
        
        # Normalize
        if norm_choosing == "yes":
            normspectrum, jump, preline, postline = normalization(energy, spectrum, edge, pre, post)
            spectrum = normspectrum

        # Adding motors positions
        if add_positions == "yes":
            spectrum = add_positioners(spectrum, goodfiles[i])
            if norm_choosing == "yes":
                spectrum = np.append(spectrum, jump)
            
        # Saving data
        file_name = goodfiles[i]
        file_name = re.sub('[!@#$/\: ]', '_', file_name)
        orange_file.append(spectrum)
        first_csv_column.append(file_name)
        
        if save_singles == "yes":
            spectrum = spectrum[:len(energy)]
            spectrum = [float(x) for x in spectrum]
            savedata(energy, spectrum, file_name)
            
        if two_plus_elem == "yes":
            if (len(energy_forcsv) < 1):
                if (((hghr_lmt_energy - lwr_lmt_energy) -(energy[-1] - energy[0])) < 0.001):
                    energy_forcsv = np.append(energy_forcsv, energy)
        else:
            if (len(energy_forcsv) < 1):
                energy_forcsv = np.append(energy_forcsv, energy)
                
        goodones += 1  # To count how many files were good and were fitted
            
    except Exception as ex:
        print("The error exception in main loop is = ", ex)
        errorones += 1
        errorfiles.append(goodfiles[i])
        continue

# Loop for saving averaged spectra
if save_average == "yes":
    avgenergylist, avgspectrumlist, avgfilenamelist = averaging()
    print()
    print("Number of files with multiple scans = ", len(avgenergylist))
    for i in range(len(avgenergylist)):    
        try: 
            # Read data
            #print("Averaged files name = ", avgfilenamelist[i])
            avgenergy = avgenergylist[i]
            avgspectrum = avgspectrumlist[i]
            avgfilename = avgfilenamelist[i]

            # Two + elements
            if two_plus_elem == "yes":
                avgcut_energy = []
                avgcut_spectrum = []
                for j in range(len(avgenergy)):
                    if ((avgenergy[j] >= lwr_lmt_energy) and (avgenergy[j] <= hghr_lmt_energy)): 
                        avgcut_energy.append(avgenergy[j]) 
                        avgcut_spectrum.append(avgspectrum[j])
                avgcut_spectrum = [float(x) for x in avgcut_spectrum]
                avgenergy = avgcut_energy
                avgspectrum = avgcut_spectrum
                avgenergy = np.array(avgenergy)
                avgspectrum = np.array(avgspectrum)
                if (len(avgenergy) < 5):
                    continue

            # Interpolating spectra
            if interpolate == "yes":
                avgenergy_range = np.arange(energy_start, energy_finish, energy_step)
                avginterp_func = Akima1DInterpolator(avgenergy, avgspectrum)
                avginterpolated_spectrum = avginterp_func(avgenergy_range)
                avgenergy = avgenergy_range
                avgspectrum = avginterpolated_spectrum
            
            # Normalize
            if norm_choosing == "yes":
                avgnormspectrum, jump, preline, postline = normalization(avgenergy, avgspectrum, edge, pre, post)
                avgspectrum = avgnormspectrum

            if matchers[0] in avgfilename:
                # Adding motors positions
                if add_positions == "yes":
                    avgspectrum = add_positioners(avgspectrum, avgfilename)
                    if norm_choosing == "yes":
                        avgspectrum = np.append(avgspectrum, jump)
                
                # Saving data
                avgfilename = re.sub('[!@#$/\: ]', '_', avgfilename)
                avgfilename = avgfilename.split("_")
                avgfilename = [element for element in avgfilename[:-1]] # Removing the last number of the name
                avgfilename = "_".join(avgfilename)

                orange_file.append(avgspectrum)
                first_csv_column.append(avgfilename)
                if save_singles == "yes":
                    avgspectrum  = avgspectrum[:len(avgenergy)]
                    avgspectrum = [float(x) for x in avgspectrum]
                    savedata(avgenergy, avgspectrum, avgfilename)
            else:
                continue
        except Exception as ex:
            print("The error exception in avg loop is = ", ex)
            errorones += 1
            errorfiles.append(avgfilename[i])
            continue

if (len(goodfiles) == 0):
    print("                THE FILE SELECTION FUNCTION FAILED, goodfiles = 0")
if (len(energy_forcsv) < 1 and (len(goodfiles) != 0)):
    print("                ADJUST THE LIMITS (lwr_lmt and hghr_lmt) TO BE INSIDE OF THE XANES SPECTRUM RANGE TO SOLVE THIS ERROR")
    
if len(energy_forcsv) > 10:
    if add_positions == "yes":
        energy_forcsv = np.append(energy_forcsv, "")
        energy_forcsv = np.append(energy_forcsv, "samy")
        energy_forcsv = np.append(energy_forcsv, "samz")
        energy_forcsv = np.append(energy_forcsv, "sampy")
        energy_forcsv = np.append(energy_forcsv, "sampz")
        if norm_choosing == "yes":
            energy_forcsv = np.append(energy_forcsv, "jump")
        orange_file.insert(0, energy_forcsv)
    else:    
        orange_file.insert(0, energy_forcsv)
    
if save_orange == "yes":
    df = pd.DataFrame(orange_file)
    df.insert(0, "Energy values", first_csv_column)
    df.to_csv(outpath + orange_name + ".csv", index=False, header=False, float_format='%10.6f')
    #np.savetxt(outpath + "orange2.csv", orange_file, fmt='%10.6f', delimiter=',') # Another method to save de .csv file
    #add_col(outpath + "orange2.csv", first_csv_column, outpath + "orange2.csv")
    
if save_excel == "yes":
    df = pd.DataFrame(orange_file)
    df.insert(0, "Energy values", first_csv_column)
    df = df.T
    df.to_csv(outpath + excel_name + ".csv", index=False, header=False, float_format='%10.6f')


# Prints with information of the good and bad files
print("")
print("Files fitted = ", goodones)
print("Bad files = " ,badones) 
print("Files with errors = " ,errorones) 
print("")
if len(badonesfil) > 0:
    print("List with bad files:")
    print(badonesfil)
    print("")
if len(errorfiles) > 0:
    print("List with error files:")
    pprint(errorfiles)
