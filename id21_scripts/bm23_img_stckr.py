"""
Example of code to stack maps from FluoXAS without doing the fit
Dependencies: python -m pip install numpy h5py pymca
"""
import os
import h5py
import numpy as np
from contextlib import contextmanager


### Input data for this code
mainh5path = "/data/visitor/hg199/bm23/20221102/EMDef2hv_xanesimaging/EMDef2hv_xanesimaging_0001/EMDef2hv_xanesimaging_0001.h5"  # Path and name of the FluoXAS h5 file
outpath    = "/data/id21/inhouse/eduardo/bm23/out/"  # Path of the directory to write the output file ("." is the current working directory)
elemdet    = "fx_det0"  # Select fluo detector with all the data 
dim1       = 17  # first dimension of the map (rows)
dim2       = 44   # second dimension of the map (columns)
energycntr = "Emono"   # Select energy counter 
outname    = "Fitted_EMDef2hv"  # Name for the output file
roistarts  = 2560 # First channel to take the roi from
roiends    = 2700 # Last channel to take the roi from
I0_corr = "I0_corr" # I0 counter to normalize (inside of "measurement")
normalization = "yes" # Write "yes" if you want to normalize the data by the given counter "I0", "no" otherwise

    
### Start of all the functions for this code
@contextmanager
def openh5(filename):
    os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
    with h5py.File(filename, mode="r") as f:
        yield f

def fileselection():
    with openh5(mainh5path) as f:
        first_keys = list(f.keys())
        #print("file name = ", first_keys)
        for i in range(len(first_keys)):
            file_name = str(first_keys[i])
            #print("file name as list = ", file_name)
            try:
                all_data_files = f[file_name + "/measurement"]
            except Exception as ex:
                print("The error exception in fileselection() is = ", ex)
                badfiles.append(file_name)
                continue

            #print("file name with measurement = ", all_data_files)
            if (elemdet in all_data_files):
                goodfiles.append(file_name)
            else:    
                badfiles.append(file_name)

def readmaps(eachfile):
    with openh5(mainh5path) as f:
        data = f[eachfile + "/measurement"]
        element = data[elemdet][:]
        newroi = 0
        for i in range(roiends-roistarts+1):
            count = roistarts + i
            newroi += element[:,count]
        #print(newroi.shape)
        energy_map = "%.4f" % f[eachfile]['instrument']['positioners'][energycntr][()]
        norm_count = data['I0_corr'][:] 
        print("Energy = ", energy_map)
        energy_map_lst.append(energy_map)
        if normalization == "yes":
            newroi = newroi / norm_count
    return newroi

def create_stack(element, i, stack):
    element = [float(x) for x in element]
    element = np.array(element)
    element = element.reshape(dim1,dim2)
    #print("element", i, element)
    if i == 0:
        stack = element
    else:
        stack = np.dstack((stack, element))
    return stack

def saveh5(stack, energy_map_lst):
    dm1 = np.arange(dim1)
    dm2 = np.arange(dim2)
    stack = np.transpose(stack, (2,0,1))

    #energy_map_lst = energy_map_lst*1000 # Remove the first "#" to uncomment the line and have the energy in ev instead of keV
    #print("Stack shape is = ", stack.shape)

    f = h5py.File(outpath + outname + ".h5", "w")  # create the HDF5 NeXus file
    nxentry = f.create_group('Results')
    nxentry.attrs["NX_class"] = 'NXentry'
    nxdata = nxentry.create_group('Stack')
    nxdata.attrs["NX_class"] = 'NXdata'

    nxdata.create_dataset('dim1', data=dm1)
    nxdata.create_dataset('dim2', data=dm2)
    nxdata.create_dataset('roi signal', data=stack)
    nxdata.create_dataset(energycntr, data=energy_map_lst)
    f.close()


# Another version to save the data as h5
def saveh5v2(stack, energy_map_lst):
    dm1 = np.arange(dim1)
    dm2 = np.arange(dim2)
    stack = np.transpose(stack, (2,0,1))
    print("Stack shape is = ", stack.shape)

    hf = h5py.File(outpath + outname + "v2.h5", 'w')
    g1 = hf.create_group('Result')
    g1.create_dataset('dim1', data=dm1)
    g1.create_dataset('dim2', data=dm2)
    g1.create_dataset('roi signal', data=stack)
    g1.create_dataset(energycntr, data=energy_map_lst)
    hf.close()
    

# Files selection and starting lists
goodfiles  = []
badfiles   = []
energy_map_lst = []

fileselection()
goodfiles = [float(x) for x in goodfiles]
goodfiles = sorted(goodfiles)
goodfiles = [str(x) for x in goodfiles]
#print("good files = ", goodfiles)

if outpath[-1] != "/":
    outpath = outpath + "/"

print("Good maps = ", len(goodfiles))
### Big loop to stack the maps
for i in range(len(goodfiles)):
    try:
        newroi = readmaps(goodfiles[i])
        #print("element", i, element)
        if i == 0:
            stack = None
        stack = create_stack(newroi, i, stack)
        #print("stack", stack)
    except Exception as ex:
        print("The error in the main loop is = ", ex)
        del energy_map_lst[i]
        badfiles.append(goodfiles[i])
        continue
    
# Create and save the output h5 file
energy_map_lst = sorted(energy_map_lst)
energy_map_lst = [float(x) for x in energy_map_lst]
energy_map_lst = np.array(energy_map_lst)  
saveh5(stack, energy_map_lst)

# Print the list of the scans with bad data
if len(badfiles) > 0:
    print("")
    print("Scans with bad data:")
    print(badfiles[1:])
    print("")
