"""
Example of code to stitch maps with different sizes to use RGB correlator tool from PyMCA as a single map.
Dependencies: python -m pip install numpy h5py pymca
"""
import os
import re
import h5py
import numpy as np
from contextlib import contextmanager


### Input data for this code

main_path  = "/data/id21/inhouse/eduardo/maps_try/"  # Path of the folder with the h5 files of the fitted maps
file_names = ["fitted_Mut_section3_roi39010_46087.h5", "fitted_Mut_section3_roi39011_46093.h5", "fitted_Mut_section3_roi39012_46098.h5", "fitted_Mut_section3_roi39013_46099.h5"] # list with all the file names of the fitted maps
outpath    = "/data/id21/inhouse/eduardo/maps_try/out/"  # Path of the directory to write the output file ("." is the current working directory)
outname    = "puzzle_final_horizontal"  # Name for the output file
vertical   = False # Stitch the images Vertically
horizontal = True  # Stitch the images horizontally


### Start of all the functions for this code
@contextmanager
def openh5(filename):
    os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
    with h5py.File(filename, mode="r") as f:
        yield f
        
        
def readmaps(file_name):
    with openh5(main_path + file_name) as f:
        data = f["/fitted_stack/normalize.1/results/parameters"]
        data_list = list(data.keys())
        for k in range(len(data)): 
            (globals()[data_list[k]]).append(f['fitted_stack']['normalize.1']['results']['parameters'][str(data_list[k])][()])
             
             
if main_path[-1] != "/":
    if main_path[-1] != ".":
        main_path = main_path + "/"
if outpath[-1] != "/":
    if outpath[-1] != ".":
        outpath = outpath + "/"
        
# Start of the main code
with openh5(main_path + file_names[0]) as f:
    data = f["/fitted_stack/normalize.1/results/parameters"]
    data_list = list(data.keys())
    print("Parameters keys", data_list)
    for i in range(len(data)):
        globals()[data_list[i]] = []

    for j in range(len(file_names)):
        print("File name = ", file_names[j])
        readmaps(file_names[j]) # Append data to the list with all the data
    
    max_lenght = 0 
    for l in range(len(file_names)):
        #print("shape", int((globals()[data_list[0]])[l].shape[0]))
        if vertical:
            if int((globals()[data_list[0]])[l].shape[1]) > max_lenght:
                max_lenght = int((globals()[data_list[0]])[l].shape[1])
        if horizontal:
            if int((globals()[data_list[0]])[l].shape[0]) > max_lenght:
                max_lenght = int((globals()[data_list[0]])[l].shape[0])
        
    print("Max_lenght final is = ", max_lenght)
        
    # Create and fill the file
    f = h5py.File(outpath + outname + ".h5", "w")  # create the HDF5 NeXus file
  
    nxentry = f.create_group('Results')
    nxentry.attrs["NX_class"] = 'NXentry'

    nxdata = nxentry.create_group('Elements')
    nxdata.attrs["NX_class"] = 'NXdata'
    
    for m in range(len(data)):
        #(globals()[data_list[m]]) = np.nan_to_num((globals()[data_list[k]])) # convert NAN to zeros
        if ((globals()[data_list[m]])[0]).ndim < 2:
            #nxdata.create_dataset(str(data_list[k]), data=(globals()[data_list[k]])[1]) # Add the dim1 and dim2 to the finl h5 file
            continue
        
        if vertical:
            
            if (len(file_names)) == 2:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[0].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[1].shape[1])), 'constant', constant_values=(0))
                
                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1]], axis=0))
            
            if (len(file_names)) == 3:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[0].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[1].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[2].shape[1])), 'constant', constant_values=(0))
                
                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2]], axis=0))
            
            if (len(file_names)) == 4:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[0].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[1].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[2].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[3].shape[1])), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3]], axis=0))
            
            if (len(file_names)) == 5:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[0].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[1].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[2].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[3].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[4].shape[1])), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4]], axis=0))
        
            if (len(file_names)) == 6:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[0].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[1].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[2].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[3].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[4].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[5] = np.lib.pad((globals()[data_list[m]])[5], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[5].shape[1])), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4], (globals()[data_list[m]])[5]], axis=0))
                
            if (len(file_names)) == 7:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[0].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[1].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[2].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[3].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[4].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[5] = np.lib.pad((globals()[data_list[m]])[5], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[5].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[6] = np.lib.pad((globals()[data_list[m]])[6], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[6].shape[1])), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4], (globals()[data_list[m]])[5], (globals()[data_list[m]])[6]], axis=0))
                
            if (len(file_names)) == 8:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[0].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[1].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[2].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[3].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[4].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[5] = np.lib.pad((globals()[data_list[m]])[5], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[5].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[6] = np.lib.pad((globals()[data_list[m]])[6], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[6].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[7] = np.lib.pad((globals()[data_list[m]])[7], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[7].shape[1])), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4], (globals()[data_list[m]])[5], (globals()[data_list[m]])[6], (globals()[data_list[m]])[7]], axis=0))
                
            if (len(file_names)) == 9:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[0].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[1].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[2].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[3].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[4].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[5] = np.lib.pad((globals()[data_list[m]])[5], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[5].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[6] = np.lib.pad((globals()[data_list[m]])[6], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[6].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[7] = np.lib.pad((globals()[data_list[m]])[7], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[7].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[8] = np.lib.pad((globals()[data_list[m]])[8], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[8].shape[1])), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4], (globals()[data_list[m]])[5], (globals()[data_list[m]])[6], (globals()[data_list[m]])[7], (globals()[data_list[m]])[8]], axis=0))
                
            if (len(file_names)) == 10:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[0].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[1].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[2].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[3].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[4].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[5] = np.lib.pad((globals()[data_list[m]])[5], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[5].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[6] = np.lib.pad((globals()[data_list[m]])[6], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[6].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[7] = np.lib.pad((globals()[data_list[m]])[7], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[7].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[8] = np.lib.pad((globals()[data_list[m]])[8], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[8].shape[1])), 'constant', constant_values=(0))
                (globals()[data_list[m]])[9] = np.lib.pad((globals()[data_list[m]])[9], ((0, 0),(0, max_lenght - (globals()[data_list[m]])[9].shape[1])), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4], (globals()[data_list[m]])[5], (globals()[data_list[m]])[6], (globals()[data_list[m]])[7], (globals()[data_list[m]])[8], (globals()[data_list[m]])[9]], axis=0))
        
        
        if horizontal:
            
            if (len(file_names)) == 2:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((max_lenght - (globals()[data_list[m]])[0].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((max_lenght - (globals()[data_list[m]])[1].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                
                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1]], axis=1))
            
            if (len(file_names)) == 3:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((max_lenght - (globals()[data_list[m]])[0].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((max_lenght - (globals()[data_list[m]])[1].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((max_lenght - (globals()[data_list[m]])[2].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                
                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2]], axis=1))
            
            if (len(file_names)) == 4:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((max_lenght - (globals()[data_list[m]])[0].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((max_lenght - (globals()[data_list[m]])[1].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((max_lenght - (globals()[data_list[m]])[2].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((max_lenght - (globals()[data_list[m]])[3].shape[0], 0),(0, 0)), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3]], axis=1))
                
            if (len(file_names)) == 5:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((max_lenght - (globals()[data_list[m]])[0].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((max_lenght - (globals()[data_list[m]])[1].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((max_lenght - (globals()[data_list[m]])[2].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((max_lenght - (globals()[data_list[m]])[3].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((max_lenght - (globals()[data_list[m]])[4].shape[0], 0),(0, 0)), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4]], axis=1))
                
            if (len(file_names)) == 6:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((max_lenght - (globals()[data_list[m]])[0].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((max_lenght - (globals()[data_list[m]])[1].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((max_lenght - (globals()[data_list[m]])[2].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((max_lenght - (globals()[data_list[m]])[3].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((max_lenght - (globals()[data_list[m]])[4].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[5] = np.lib.pad((globals()[data_list[m]])[5], ((max_lenght - (globals()[data_list[m]])[5].shape[0], 0),(0, 0)), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4], (globals()[data_list[m]])[5]], axis=1))
                
            if (len(file_names)) == 7:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((max_lenght - (globals()[data_list[m]])[0].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((max_lenght - (globals()[data_list[m]])[1].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((max_lenght - (globals()[data_list[m]])[2].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((max_lenght - (globals()[data_list[m]])[3].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((max_lenght - (globals()[data_list[m]])[4].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[5] = np.lib.pad((globals()[data_list[m]])[5], ((max_lenght - (globals()[data_list[m]])[5].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[6] = np.lib.pad((globals()[data_list[m]])[6], ((max_lenght - (globals()[data_list[m]])[6].shape[0], 0),(0, 0)), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4], (globals()[data_list[m]])[5], (globals()[data_list[m]])[6]], axis=1))
                
            if (len(file_names)) == 8:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((max_lenght - (globals()[data_list[m]])[0].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((max_lenght - (globals()[data_list[m]])[1].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((max_lenght - (globals()[data_list[m]])[2].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((max_lenght - (globals()[data_list[m]])[3].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((max_lenght - (globals()[data_list[m]])[4].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[5] = np.lib.pad((globals()[data_list[m]])[5], ((max_lenght - (globals()[data_list[m]])[5].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[6] = np.lib.pad((globals()[data_list[m]])[6], ((max_lenght - (globals()[data_list[m]])[6].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[7] = np.lib.pad((globals()[data_list[m]])[7], ((max_lenght - (globals()[data_list[m]])[7].shape[0], 0),(0, 0)), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4], (globals()[data_list[m]])[5], (globals()[data_list[m]])[6], (globals()[data_list[m]])[7]], axis=1))
                
            if (len(file_names)) == 9:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((max_lenght - (globals()[data_list[m]])[0].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((max_lenght - (globals()[data_list[m]])[1].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((max_lenght - (globals()[data_list[m]])[2].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((max_lenght - (globals()[data_list[m]])[3].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((max_lenght - (globals()[data_list[m]])[4].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[5] = np.lib.pad((globals()[data_list[m]])[5], ((max_lenght - (globals()[data_list[m]])[5].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[6] = np.lib.pad((globals()[data_list[m]])[6], ((max_lenght - (globals()[data_list[m]])[6].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[7] = np.lib.pad((globals()[data_list[m]])[7], ((max_lenght - (globals()[data_list[m]])[7].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[8] = np.lib.pad((globals()[data_list[m]])[8], ((max_lenght - (globals()[data_list[m]])[8].shape[0], 0),(0, 0)), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4], (globals()[data_list[m]])[5], (globals()[data_list[m]])[6], (globals()[data_list[m]])[7], (globals()[data_list[m]])[8]], axis=1))
                
            if (len(file_names)) == 10:
                (globals()[data_list[m]])[0] = np.lib.pad((globals()[data_list[m]])[0], ((max_lenght - (globals()[data_list[m]])[0].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[1] = np.lib.pad((globals()[data_list[m]])[1], ((max_lenght - (globals()[data_list[m]])[1].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[2] = np.lib.pad((globals()[data_list[m]])[2], ((max_lenght - (globals()[data_list[m]])[2].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[3] = np.lib.pad((globals()[data_list[m]])[3], ((max_lenght - (globals()[data_list[m]])[3].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[4] = np.lib.pad((globals()[data_list[m]])[4], ((max_lenght - (globals()[data_list[m]])[4].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[5] = np.lib.pad((globals()[data_list[m]])[5], ((max_lenght - (globals()[data_list[m]])[5].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[6] = np.lib.pad((globals()[data_list[m]])[6], ((max_lenght - (globals()[data_list[m]])[6].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[7] = np.lib.pad((globals()[data_list[m]])[7], ((max_lenght - (globals()[data_list[m]])[7].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[8] = np.lib.pad((globals()[data_list[m]])[8], ((max_lenght - (globals()[data_list[m]])[8].shape[0], 0),(0, 0)), 'constant', constant_values=(0))
                (globals()[data_list[m]])[9] = np.lib.pad((globals()[data_list[m]])[9], ((max_lenght - (globals()[data_list[m]])[9].shape[0], 0),(0, 0)), 'constant', constant_values=(0))

                nxdata.create_dataset(str(data_list[m]), data=np.concatenate([(globals()[data_list[m]])[0], (globals()[data_list[m]])[1], (globals()[data_list[m]])[2], (globals()[data_list[m]])[3], (globals()[data_list[m]])[4], (globals()[data_list[m]])[5], (globals()[data_list[m]])[6], (globals()[data_list[m]])[7], (globals()[data_list[m]])[8], (globals()[data_list[m]])[8]], axis=1))
    
    f.close()
       
